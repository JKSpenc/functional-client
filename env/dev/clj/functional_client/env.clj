(ns functional-client.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [functional-client.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[functional-client started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[functional-client has shut down successfully]=-"))
   :middleware wrap-dev})
