FROM openjdk:8-alpine

COPY target/uberjar/functional-client.jar /functional-client/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/functional-client/app.jar"]
