(ns functional-client.solver)

(defn- get-neighbours
  [[x y] grid]
  (filter #(get-in grid (last %)) [["up" [(- x 1) y]]
                                   ["left" [x (- y 1)]]
                                   ["down" [(+ x 1) y]]
                                   ["right" [x (+ y 1)]]
                                   ]
          )
  )

(def get-neighbours-memo (memoize get-neighbours))

(defn- squared [^Number x]
  (* x x)
  )

(defn- euclidean-distance [[x1 y1] [x2 y2]]
  (Math/sqrt (+ (squared (- x1 x2)) (squared (- y1 y2))))
  )

(defn- get-possible-moves [cell directions maze]
  (map #(last %) (filter #(not= -1 (.indexOf (vec (keys directions)) (first %))) (get-neighbours-memo cell maze))))

(def get-possible-moves-memo (memoize get-possible-moves))

(defn- process-children [current neighbours open-list closed end]
  (loop [
         open open-list
         neighbours neighbours
         ]
    (let
      [child (first neighbours)]
      (cond
        (empty? (vec neighbours)) open
        (not= (.indexOf (map #(% :position) closed) child) -1) (recur open (rest neighbours))
        :else (let
                [
                 g (+ (current :g) 1)
                 h (euclidean-distance child end)
                 f (+ g h)
                 openListPositions (map #(% :position) open)
                 ]
                (if (not= (.indexOf openListPositions child) -1)
                  (let [
                        existing-node (nth open (.indexOf openListPositions child))
                        ]
                    (if (> g (existing-node :g))
                      (recur open (rest neighbours))
                      (recur (cons {:parent current :position child :g g :h h :f f} open) (rest neighbours))
                      )
                    )
                  (recur (cons {:parent current :position child :g g :h h :f f} open) (rest neighbours))
                  )
                )
        )
      )
    )
  )

(def process-children-memo (memoize process-children))

(defn- backtrack [nested-node]
  (loop [
         parent (nested-node :parent)
         path [(nested-node :position)]
         ]
    (if (nil? parent)
      path
      (recur (parent :parent) (cons (parent :position) path))
      )
    )
  )

(defn solve
  [maze start end]
  (loop
    [
     open [{:parent nil :position start :g 0 :h 0 :f 0}]
     closed []
     ]
    (if (empty? open)
      (backtrack (first closed))
      (let [
            current (first (sort-by #(% :f) < open))
            neighbours (get-possible-moves-memo (current :position)
                                                (get-in maze (current :position))
                                                maze)
            open-list-current-removed (remove #(= % current) open)
            closed-list-current-added (cons current closed)
            ]
        (if (= (current :position) end)
          (recur [] closed-list-current-added)
          (recur (process-children-memo current
                                        neighbours
                                        open-list-current-removed
                                        closed-list-current-added end)
                 closed-list-current-added)
          )
        )
      )
    )
  )