(ns functional-client.routes.home
  (:require
    [functional-client.layout :as layout]
    [clojure.java.io :as io]
    [clojure.string :as str]
    [functional-client.middleware :as middleware]
    [ring.util.response]
    [ring.util.http-response :as response]
    [clj-http.client :as client]
    [cheshire.core :refer :all]
    [functional-client.solver :as s]
    [selmer.filters :refer :all]
    [selmer.parser :refer :all]
    ))

(defn string-to-coordinate [^String str]
  (map #(Integer/parseInt %) (str/split str #","))
  )

(defn coordinate-to-string [coord]
  (str (first coord) "," (last coord))
  )

(defn cell-in-solution [solution cell]
  (if (false? solution)
    ""
    (let [index (.indexOf solution cell)]
      (if (= index -1)
        ""
        (cond
          (= index 0) "start"
          (= index (dec (count solution))) "end"
          :else "path"
          ))
      )
    )
  )



(defn delete-maze [request]
  (let [id ((request :path-params) :id)]
    (client/delete (str "http://localhost:3000/delete/" id))
    )
  )

(defn home-page [request]
  (let [
        mazes (parse-string
                (get
                  (client/get
                    "http://localhost:3000/mazes"
                    {:accept :json}) :body))
        ]
  (layout/render request "home.html" {:mazes mazes}))
  )

(defn new-maze [request]
  (let [
        form-params (request :form-params)
        height (form-params "height")
        width (form-params "width")
        name (form-params "name")
        ]
    (client/post "http://localhost:3000/gen-maze" {:query-params {:height height :width width :name name}})
    (home-page request)
    )
  )


(defn maze-page [request]
  (add-filter! :right? #(get % "right"))
  (add-filter! :down? #(get % "down"))
  (let [
        form-params (request :form-params)
        start (if (= (request :request-method) :get) nil (string-to-coordinate (form-params "start")))
        end (if (= (request :request-method) :get) nil (string-to-coordinate (form-params "end")))
        response (parse-string
                   (get
                     (client/get
                       (str "http://localhost:3000/maze/" ((request :path-params) :id))
                       {:accept :json}) :body))
        id (response "_id")
        maze (response "maze")
        ratio (/ (count maze) (count (first maze)))
        height (if (> (count maze) (count (first maze))) (Math/floor (* 500 ratio)) 500)
        width (if (< (count maze) (count (first maze))) (Math/floor (* 500 (/ 1 ratio))) 500)
        solution (if (= (request :request-method) :get) false (s/solve maze start end))
        ]
    (add-tag! :in-solution (fn [_ _ content]
                             (cell-in-solution solution (string-to-coordinate (str/trim ((content :in-solution) :content)))))
              :solution :endsolution)
    (layout/render request "maze.html" {:id    id :maze maze :solution solution
                                        :start (coordinate-to-string start)
                                        :end   (coordinate-to-string end)
                                        :height height
                                        :width width}))
  )

(defn about-page [request]
  (layout/render request "about.html"))

(defn home-routes []
  [""
   {:middleware [middleware/wrap-csrf
                 middleware/wrap-formats]}
   ["/" {:get home-page :post new-maze}]
   ["/maze/:id" {:get maze-page :post maze-page}]
   ["/delete/:id" {:get delete-maze}]])

